var webpackConfig = require('./webpack.config');

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai', 'sinon'],
    files: [
      './test/*.spec.ts',
      {pattern: './dist/*', watched: false, included:false, served: true},
      {pattern: './dist/lib/*', watched: false, included:false, served: true}
    ],
    proxies: {
		'/dist': '/base/dist'
	},
    exclude: [
		'./dist/master.js'
    ],
    preprocessors: {
      'test/**/*.ts': ['webpack']
    },
    webpack: {
      module: webpackConfig.module,
      resolve: webpackConfig.resolve,
      plugins: webpackConfig.plugins
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome','Firefox'],
    singleRun: false,
    concurrency: Infinity,
    browserNoActivityTimeout: 100000
  })
}

