
/**
 * Scss and css
 */
import './node_modules/bootstrap/dist/css/bootstrap.min.css';

/**
 * Js
 */
import './node_modules/angular2/bundles/angular2-polyfills.js';
import './node_modules/bootstrap/dist/js/bootstrap.min.js';


import {provide} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import Passit from './passit/passit';
import Controller from './passit/controller/controller';
import Master from 'simple-asymmetric-javascript';
import Db from './passit/controller/db';
import User from './passit/controller/user';
import Api from './passit/controller/api';
import Message from './passit/controller/message';

bootstrap(Passit, [ROUTER_PROVIDERS, provide(LocationStrategy, {useClass: HashLocationStrategy}), Controller, Master, Db, User, Api, HTTP_PROVIDERS, Message]);
