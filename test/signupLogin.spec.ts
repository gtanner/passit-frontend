
import controller from './controller';
import {signup} from '../passit/security/signup';
import {login} from '../passit/security/login';
import { expect } from 'chai';

function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

var email = makeid() + '@karama.com';
var pass = 'testpassword';

describe('Signup Login Test', function(){
    this.timeout(100000);
    it('Should Signup and Login', function(done){
        this.timeout(100000);
        signup(controller, email, pass).then(function(){
            login(controller, email, pass, false).then(function(){
                var stat = controller.user.getInfo();
                expect(stat.email).to.be.equal(email);
                done();
            });
        });
    });
});
