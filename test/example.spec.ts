

import controller from './controller';
import { expect } from 'chai';

/**
 * Example Test
 */
describe('Example', function(){
    it('Should Work', function(){
        var stat = controller.user.getInfo();
        expect(stat.id).to.be.equal(0);
    });
});
