
import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {Control, ControlGroup, Validators} from 'angular2/common';
import Controller from '../controller/controller';
import {urlValidator} from '../include/validator';

@Component({
    template: require('../template/secrets.html'),
    styleUrls: ['../css/secrets.css']    
})

export default class Secrets {

    /**
     * The secret search control
     * @type {Control}
     */
    public secretSearchControl: Control = new Control('', Validators.required);

    /**
     * The site name control
     * @type {Control}
     */
    public siteNameControl: Control = new Control('', Validators.required);

    /**
     * The url control
     * @type {Control}
     */
    public urlControl: Control = new Control('', urlValidator);

    constructor(private _router:Router, private _controller:Controller){

    }

}
