
/**
 * Create a cookie
 * @param {string} name - the name of the cookie
 * @param {string} value - the value of the cookie
 * @param {number|null} days - the number of days or null for a session cookie
 */
export var createCookie = (name:string, value:string, days?:number): void =>{
    var cookie = name + "=" + value + ";";
    if(days){
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toUTCString();
    } else {
        var expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
};


/**
 * Get a cookie
 * @param {string} name - the name of the cookie
 * @return {string|null} the value of the cookie or null for not found
 */
export var getCookie = (name:string): any =>{
    var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
    var result = regexp.exec(document.cookie);
    return (result === null) ? null : result[1];
};
