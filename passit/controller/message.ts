
import {Injectable} from 'angular2/core';

@Injectable()
/**
 * Class for displaying status messages
 */
export default class Message {

    /**
     * The selector of the dialog
     * @type {Object}
     */
    private _selector: any;

    /**
     * The template html for status messages
     * @type {object}
     */
    private _messageHTML: any = require('../template/message/status.nunj');

    /**
     * The current id of the dialog
     * @type {number}
     */
    private _modalId: number = 0;

    /**
     * The timeout for the status messages
     * @type {Timeout}
     */
    private _timeout: any = null;

    constructor(){
        
    }

    /**
     * Close the modal and clear the message html
     */
    public clear(): void {
        if(this._timeout){
            clearTimeout(this._timeout);
            this._timeout = null;
        }
        var tmp:any = this._modalId - 1;
        $('#messageModal-' + tmp).modal('hide');
        this._modalId++;
    }
    
    /**
     * Show a status message
     * @param {string} title - the message title
     * @param {string} body - the message body
     * @param {number|null|undefined} ms - the total time for the message popup or none for unlimited
     */
    showStatus(title:string, body:string, ms?: number): void {
        var templateObject: messageTemplateObject = {
            title: title,
            body: body,
            modalId: 'messageModal-' + this._modalId
        };
        if(ms){
            templateObject.close = true;
        }
        var tpl = this._messageHTML.render(templateObject);
        this.clear();
        var subTime = setTimeout(()=>{
            this._selector = $('message');
            this._selector.html(tpl);
            var tmp:any = this._modalId - 1;
            $('#messageModal-' + tmp).modal({backdrop:'static', keyboard:false});
        }, 500);
        if(ms){
            this._timeout = setTimeout(()=>{
                this.clear();
            },ms);
        }
    }

};
