
import {Injectable} from 'angular2/core';
import Master from 'simple-asymmetric-javascript';
import Db from './db';
import User from './user';
import Api from './api';
import Message from './message';

@Injectable()
/**
 * Class for handeling the various modules in the app
 */
export default class Controller {

    /**
     * @param {Master} asym - the class for encryption and decrytpion
     * @param {Db} db - the collection of datastores
     * @param {User} user - the users info
     * @param {Api} api - the http connection to the backend
     * @param {Message} message - the status message
     */
    constructor(public asym:Master, public db:Db, public user:User, public api:Api, public message:Message){

    }

};
