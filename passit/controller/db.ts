
import {Injectable} from 'angular2/core';

@Injectable()
/**
 * Class for managing the localStorage db
 */
export default class Db {

    /**
     * The database for the secrets
     * @type {nedb}
     */
    public secretDb: any;

    /**
     * The database for the groups
     * @type {nedb}
     */
    public groupDb: any;

    constructor(){
        this.secretDb = new Nedb({filename:'secrets.db'});
        this.groupDb = new Nedb({filename:'groups.db'});
        this.create();
    }

    /**
     * Create the databases
     */
    create(): void {
       this.secretDb.loadDatabase();
       this.groupDb.loadDatabase();
    }

    /**
     * Destroy the datastores
     * @return {Promise} when the delete is done
     */
    destroy(): any {
        return new Promise((resolve, reject)=>{ 
            this.secretDb.remove({ }, { multi: true }, (err, numRemoved)=>{
                this.groupDb.remove({ }, { multi: true }, (err, numRemoved)=>{
                    this.create();
                    return resolve();
                });
            });
        });
    }

};
