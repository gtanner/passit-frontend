
import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import User from './user';

@Injectable()
/**
 * Class for connection to the api
 */
export default class Api {

    /**
     * The base url of the api
     * @type {string}
     */
    private _baseUrl: string = 'http://0.0.0.0:8000';

    /**
     * Multi mode for sending mutiple async authenticated requests
     * @type {boolean}
     */
    private _multi: boolean = false;

    /**
     * Init
     * @param {Http} http - the angular2 http class
     * @param {User} user - the information about the current user
     */
    constructor(private _http:Http, private _user:User){

    }

    /**
     * Resave the users cookies
     */
    private _saveCookies(): void {
        if(!this._multi){
            var days = this._user.get('days');
            if(!days){
                this._user.save();
            } else {
                this._user.save(days);
            }
        }
    }

    /**
     * Enable the multi mode
     */
    public enableMulti(): void {
        this._multi = true;
        this._user.deleteCookie();
    }

    /**
     * Disable the multi mode
     */
    public disableMulti(): void {
        this._multi = false;
        this._saveCookies();
    }

    /**
     * Make a json post
     * @param {string} uri - the url of the to post to append to the base url
     * @param {object} body - the json body
     * @return {Event} the http response
     */
    public jsonPost(uri:string, body:any): any {
        var headers = new Headers();
        headers.append('Content-Type','application/json');
        headers.append('Accept','application/json');
        return this._http.post(this._baseUrl + uri, JSON.stringify(body), {headers:headers})
    }

    /**
     * Make a json get request
     * @param {string} uri - the url of the get request
     * @return {Event} the http response
     */
    public jsonGet(uri:string): any {
        var headers = new Headers();
        headers.append('Accept','application/json');
        return this._http.get(this._baseUrl + uri, {headers:headers});
    }

    /**
     * Login request
     * @param {string} uri - the url of the login request
     * @param {string} email - the users email
     * @param {string} hash - the users hash
     * @return {Event} the http response
     */
    public login(uri:string, email:string, hash:string): any {
        var auth = new Headers();
        auth.append('Accept','application/json');
        var base = btoa(email + ':' + hash);
        auth.append('Authorization','Basic ' + base);
        return this._http.post(this._baseUrl + uri, "", {headers:auth});
    }

    /**
     * Send a request that requires auth
     * @param {string} method - the method of the request
     * @param {string} uri - the uri of thre request
     * @param {object} body - the body of the request
     * @return {Promise} the http request
     */
    private _auth(method:string, uri:string, body?:any): any{
        if(!this._user.authenticated()){
            throw new Error('The User Is Not Logged In Cannot Complete The Request');
        }
        var headers = new Headers();
        headers.append('Content-Type','application/json');
        headers.append('Accept','application/json');
        headers.append('Authorization','Token ' + this._user.get('token'));
        //delete the users cookie before the request
        if(!this._multi){
            this._user.deleteCookie();
        }
        //send the request
        var requestEvent;
        switch(method){
            case 'GET':
                requestEvent = this._http.get(this._baseUrl + uri, {headers: headers});
                break;
            case 'POST':
                requestEvent = this._http.post(this._baseUrl + uri, JSON.stringify(body), {headers: headers});
                break;
            case 'PUT':
                requestEvent = this._http.put(this._baseUrl + uri, JSON.stringify(body), {headers: headers});
                break;
            case 'DELETE':
                requestEvent = this._http.delete(this._baseUrl + uri, {headers: headers});
                break;
            case 'PATCH':
                requestEvent = this._http.patch(this._baseUrl + uri, JSON.stringify(body), {headers: headers});
                break;
            case 'HEAD':
                requestEvent = this._http.head(this._baseUrl + uri, {headers: headers});
                break;
            default:
                throw new Error('Http Method Not Defined');
        }
        
        return new Promise((resolve, reject)=>{ 
            requestEvent.subscribe(res => {
                this._saveCookies();    
                resolve(res);
            }, err=>{
                this._saveCookies();
                reject(err);
            })
        });
    }

    /**
     * Send an authenticated get request
     * @param {string} uri - the uri to send the get request to
     * @return {Promise} the http response
     */
    public get(uri:string): any {
        return this._auth('GET', uri);
    }

    /**
     * Send an authenticated post request
     * @param {string} uri - the uri to send the post request to
     * @param {object} body - the body of the request
     * @return {Promise} the http response
     */
    public post(uri:string, body?:any): any {
        return this._auth('POST', uri, body);
    }

    /**
     * Send an authenticated put request
     * @param {string} uri - the uri to send the put request to
     * @param {object} body - the body of the request
     * @return {Promise} the http response
     */
    public put(uri:string, body?:any): any {
        return this._auth('PUT', uri, body);
    }

    /**
     * Send an authenticated delete request
     * @param {string} uri - the uri to send the delete request
     * @return {Promise} the http response
     */
    public delete(uri:string): any {
        return this._auth('DELETE', uri);
    }

    /**
     * Send an authenticated path request
     * @param {string} uri - the uri to send the path request
     * @param {object} body - the body of the request
     * @return {Promise} the http response
     */
    public patch(uri:string, body?:any): any {
        return this._auth('PATCH', uri, body);
    }

    /**
     * Send an authenticated head request
     * @param {string} uri - the uri to send the head request to
     * @return {Promise} the http response
     */
    public head(uri:string): any {
        return this._auth('HEAD', uri);
    }

};
