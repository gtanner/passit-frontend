

import Controller from '../controller/controller';

/**
 * Sign the user up
 * @param {Controller} controller - the app controller
 * @param {string} email - the users email
 * @param {string} password - the users password
 * @return {Promise}
 */
export var signup = (controller:Controller, email:string, password:string): any =>{
    return new Promise((resolve, reject)=>{
        //check if the email exists
        controller.api.jsonGet(`/api/username-available/${email}`).subscribe(res =>{
            res = res.json();
            if(!res.available){
                controller.message.showStatus('Sign Up Error', 'This email has been used before, please use a different email address', 4000); 
                return reject();
            }
            var keys;
            //create the users keys
            controller.asym.exec('make_rsa_keys', {passphrase: password}).then((keyObj)=>{
                keys = keyObj;
                //create the users hash
                return controller.asym.exec('generate_hash', {password: password, numIterations: 24000});
            }).then((hashObj)=>{
                controller.api.jsonPost('/api/users/',{
                        email: email,
                        password: hashObj.hash,
                        private_key: keys.private_key,
                        public_key: keys.public_key,
                        client_salt: hashObj.salt
                    }).subscribe(res =>{
                        return resolve();
                    }, err =>{
                        controller.message.showStatus('Sign Up Error', 'Unable to sign up', 4000); 
                        return reject();
                    });
            });
        }, err =>{
           controller.message.showStatus('Sign Up Error', 'This email has been used before, please use a different email address', 4000); 
           return reject();
        });

    });
};
