
import Controller from '../controller/controller';

/**
 * Create a new group
 * @param {Controller} controller - the app controller
 * @param {string} name - the group name
 * @param {string} slug - the group slug
 * @return {Promise} when complete resolve
 */
export var createGroup = (controller:Controller, name:string, slug:string): any =>{
    return new Promise((resolve, reject)=>{
        /**
         * The users public key in an object
         * @type {object}
         */
        var publicKey;
        /**
         * The group public and private key
         * @type {object}
         */
        var keys;
        /**
         * The group aes key
         * @type {object}
         */
        var aesKey;
        //get the users public key
        controller.asym.exec('public_key').then((publicKeyObj)=>{
            publicKey = publicKeyObj;
            //generate the group keys
            return controller.asym.exec('make_rsa_keys');
        }).then((keyObj)=>{
            keys = keyObj;
            //make a new group aes key
            return controller.asym.exec('make_aes_key');
        }).then(()=>{
            //encrypt the aes key with the users public key
            return controller.asym.exec('get_encrypted_aes_key', {public_key: publicKey.public_key, use_base64:true});
        }).then((aesKeyObj)=>{
            aesKey = aesKeyObj;
            //encrypt the private key
            return controller.asym.exec('encrypt', {text: keys.private_key});
        }).then((encryptedObj)=>{
            //send to the server
            controller.api.post('/api/groups/', {
                name: name,
                slug: slug,
                public_key: keys.public_key,
                encrypted_aes_key: aesKey.encrypted_key,
                encrypted_private_key: encryptedObj.ciphertext
            }).then((res)=>{
                res = res.json();
                //add the groups to the group db
                controller.db.groupDb.insert(res, (err, doc)=>{
                    if(err){
                        return reject(err);
                    }
                    //reload the users keys
                    controller.user.load_keys();
                    return resolve(res);
                });
            }).catch((err)=>{
                return reject(err.json());
            });
        });
    });
};

/**
 * Add a single member to a group
 * @param {Controller} controller - the app controller
 * @param {string} member - the members email address
 * @return {Promise}
 */
export var addMember = (controller:Controller, member:string): any =>{

};

/**
 * Add members to a group
 * @param {Controller} controller - the app controller
 * @param {string[]} members - the emails of the members in an array
 * @return {Promise}
 */
export var addMembers = (controller:Controller, members:string[]): any =>{

};
